﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GitTool
{
    /// <summary>
    /// Logique d'interaction pour OptionsWindow.xaml
    /// </summary>
    public partial class OptionsWindow : Window
    {
        public OptionsWindow()
        {
            InitializeComponent();
        }

        private void SaveCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string directory = DirectoryTextBox.Text;
            if (!directory[directory.Length - 1].Equals(System.IO.Path.DirectorySeparatorChar))
            {
                directory += System.IO.Path.DirectorySeparatorChar;
            }

            Properties.Settings.Default.Directory = directory;
            Properties.Settings.Default.GitExe = GitExeTextBox.Text;
            Properties.Settings.Default.UserName = UserNameTextBox.Text;
            Properties.Settings.Default.UserEmail = UserEmailTextBox.Text;
            Properties.Settings.Default.Save();

            // Réactualisation de la liste des dépôts
            ((MainWindow)Owner).ScanDirectory();

            // Fermeture de la fenêtre
            Close();
        }

        private void CloseCommandBinding(object sender, ExecutedRoutedEventArgs e)
        {
            // Fermeture de la fenêtre
            Close();
        }

        private void DirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = System.IO.Path.GetDirectoryName(DirectoryTextBox.Text);

            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryTextBox.Text = dialog.SelectedPath;
            }
        }

        private void GitExeButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = System.IO.Path.GetDirectoryName(GitExeTextBox.Text);
            dialog.Filter = "Exécutables Git (git.exe)|git.exe";

            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                GitExeTextBox.Text = dialog.FileName;
            }
        }
    }
}
