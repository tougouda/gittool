﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GitTool
{
    /// <summary>
    /// Logique d'interaction pour RenameWindow.xaml
    /// </summary>
    public partial class RenameWindow : Window
    {
        /// <summary>
        /// Obtient ou définit le nom actuel de la branche à renommer.
        /// </summary>
        public string CurrentBranch { get; set; }

        public RenameWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void SaveCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Renommage de la branche
            ((MainWindow)Owner).RenameCurrentBranch(BranchTextBox.Text);

            // Fermeture de la fenêtre
            Close();
        }

        private void CloseCommandBinding(object sender, ExecutedRoutedEventArgs e)
        {
            // Fermeture de la fenêtre
            Close();
        }
    }
}
