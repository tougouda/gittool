﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitTool
{
    public class Repository
    {
        public string Name { get; set; }

        public string Path { get; set; }

        private List<Repository> repositories = new List<Repository>();
        public List<Repository> Repositories
        {
            get { return repositories; }
            set { repositories = value; }
        }
    }
}
