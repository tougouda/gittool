﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GitTool
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Constants

        /// <summary>
        /// La branche master.
        /// </summary>
        private static string BRANCH_MASTER = "master";

        /// <summary>
        /// La branche hotfixes.
        /// </summary>
        private static string BRANCH_HOTFIXES = "hotfixes";

        /// <summary>
        /// La branche staging.
        /// </summary>
        private static string BRANCH_STAGING = "staging";

        /// <summary>
        /// Le nom de la configuration pour le nom de l'utilisateur.
        /// </summary>
        private static string CONFIG_USER_NAME = "user.name";

        /// <summary>
        /// Le nom de la configuration pour l'adresse e-mail de l'utilisateur.
        /// </summary>
        private static string CONFIG_USER_EMAIL = "user.email";

        /// <summary>
        /// Le nom de la configuration pour les fins de lignes automatiquement en CR+LF.
        /// </summary>
        private static string CONFIG_AUTOCRLF = "core.autocrlf";

        /// <summary>
        /// Le nom de la configuration pour que les pulls effectués sur les nouvelles branches soient en mode "rebase".
        /// </summary>
        private static string CONFIG_BRANCH_AUTOSETUPREBASE = "branch.autosetuprebase";

        /// <summary>
        /// Le nom de la configuration pour que tous les pulls soient effectués en mode "rebase".
        /// </summary>
        private static string CONFIG_PULL_REBASE = "pull.rebase";
        #endregion

        private bool isWorking = true;
        private List<Repository> repositoriesGroups = new List<Repository>();
        private bool isRepositoryReady = false;
        private string version;
        private string currentBranch;
        private bool dryRun = false;
        private Dictionary<string, ArrayList> chmod = new Dictionary<string, ArrayList>();

        /// <summary>
        /// Les types de sortie sur la console.
        /// </summary>
        public enum OutputType {
            /// <summary>
            /// Une sortie standard.
            /// </summary>
            Standard,

            /// <summary>
            /// Une erreur.
            /// </summary>
            Error,

            /// <summary>
            /// Une commande.
            /// </summary>
            Command
        };

        #region Properties

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Obtient ou définit le numéro de version de l’application.
        /// </summary>
        public string ApplicationVersion
        {
            get
            {
                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                {
                    return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                }
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        /// <summary>
        /// Obtient ou définit une valeur qui indique si l'application est en train de travailler.
        /// </summary>
        public bool IsWorking
        {
            get
            {
                return isWorking;
            }
            set
            {
                isWorking = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Obtient ou définit la liste des groupes de dépôts.
        /// </summary>
        public List<Repository> RepositoriesGroups
        {
            get
            {
                return repositoriesGroups;
            }
            set
            {
                repositoriesGroups = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Obtient ou définit une valeur qui indique si le dépôt sélectionné est prêt.
        /// </summary>
        public bool IsRepositoryReady
        {
            get
            {
                return isRepositoryReady;
            }
            set
            {
                isRepositoryReady = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Obtient ou définit le numéro de version.
        /// </summary>
        public string Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Obtient ou définit le nom de la branche en cours.
        /// </summary>
        public string CurrentBranch
        {
            get
            {
                return currentBranch;
            }
            set
            {
                currentBranch = value;
                SetCurrentBranch();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Obtient ou définit une valeur qui indique si le mode Dry run est activé ou non.
        /// </summary>
        public bool DryRun
        {
            get
            {
                return dryRun;
            }
            set
            {
                dryRun = value;
                OnPropertyChanged();
            }
        }

        private void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            OutputTextBox.Document.Blocks.Clear();

            chmod.Add("add", new ArrayList());
            chmod.Add("remove", new ArrayList());

            ScanDirectory();

            IsWorking = false;
        }

        /// <summary>
        /// Parcourt les répertoires pour lister les dépôt Git.
        /// </summary>
        public void ScanDirectory()
        {
            IsWorking = true;
            IsRepositoryReady = false;

            // Réactualisation de la liste des dépôts dans un thread séparé
            ThreadStart ths = new ThreadStart(delegate ()
            {
                Dispatcher.Invoke((Action)delegate ()
                {
                    StatusTextBlock.Text = "Récupération des dépôts...";

                    BranchesComboBox.Items.Clear();
                    BranchesToMergeComboBox.Items.Clear();
                });

                List<Repository> repositoriesGroups = new List<Repository>();
                Dictionary<string, List<Repository>> treeViewElements = new Dictionary<string, List<Repository>>();

                try
                {
                    string[] directories = Directory.GetDirectories(Properties.Settings.Default.Directory as string, ".git", SearchOption.AllDirectories);

                    foreach (string element in directories)
                    {
                        if (element.IndexOf(System.IO.Path.DirectorySeparatorChar + "vendor" + System.IO.Path.DirectorySeparatorChar) == -1)
                        {
                            string path = Regex.Replace(System.IO.Path.GetDirectoryName(element), "^" + Regex.Escape(Properties.Settings.Default.Directory as string), "");

                            string root = System.IO.Path.GetFileName(path);
                            int firstSeparatorIndex = path.IndexOf(System.IO.Path.DirectorySeparatorChar);
                            if (firstSeparatorIndex > -1)
                            {
                                root = path.Substring(0, firstSeparatorIndex);
                            }

                            Repository repositoryGroup = null;
                            foreach (Repository group in repositoriesGroups)
                            {
                                if (group.Name.Equals(root))
                                {
                                    repositoryGroup = group;
                                    break;
                                }
                            }

                            if (repositoryGroup == null)
                            {
                                repositoryGroup = new Repository();
                                repositoryGroup.Name = root;
                                repositoriesGroups.Add(repositoryGroup);
                            }

                            Repository repository = new Repository();
                            repository.Path = System.IO.Path.GetDirectoryName(element);
                            repository.Name = System.IO.Path.GetFileName(repository.Path);
                            repositoryGroup.Repositories.Add(repository);
                        }
                    }
                }
                catch (Exception e)
                {
                }

                RepositoriesGroups = repositoriesGroups;

                Dispatcher.Invoke((Action)delegate ()
                {
                    StatusTextBlock.Text = "Prêt";
                });

                IsWorking = false;
            });

            Thread th = new Thread(ths);
            th.Start();
        }

        /// <summary>
        /// Exécute une commande Git.
        /// </summary>
        /// <param name="repository">Le dépôt Git.</param>
        /// <param name="command">La commande à exécuter.</param>
        /// <param name="dataReceivedEventHandler">La fonction à exécuter lors de la réception de chaque ligne de la sortie standard de la commande.</param>
        /// <param name="beginAction">L'action à exécuter avant l'exécution de la commande.</param>
        /// <param name="endAction">L'action à exécuter lorsque la commande est terminée.</param>
        /// <param name="label">Le libellé à afficher dans la barre d’état pendant l’exécution de la commande.</param>
        private void ExecGitCommand(Repository repository, string command, Action beginAction = null, DataReceivedEventHandler dataReceivedEventHandler = null, Action endAction = null, string label = null)
        {
            if (label != null)
            {
                StatusTextBlock.Text = label;
            }
            beginAction?.Invoke();

            ProcessStartInfo psi = new ProcessStartInfo("cmd.exe");
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.RedirectStandardInput = true;
            if (repository != null)
            {
                psi.WorkingDirectory = repository.Path;
            }
            psi.Arguments = String.Format(@"/C """"{0}"" {1}""", Properties.Settings.Default.GitExe, command);

            Process p = new Process();
            p.StartInfo = psi;

            if (DryRun)
            {
                // Juste l'affichage de la commande si on est en mode Dry run
                OutputTextAppendLine(p.StartInfo.FileName + "\" " + p.StartInfo.Arguments, OutputType.Command);
                Dispatcher.Invoke((Action)delegate ()
                {
                    if (endAction != null)
                    {
                        endAction();
                    }
                    else
                    {
                        StatusTextBlock.Text = "Prêt";
                        IsWorking = false;
                        IsRepositoryReady = true;
                    }
                });
            }
            else
            {
                // Événement lors de la réception de données sur la sortie standard
                p.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
                {
                    Dispatcher.Invoke((Action)delegate ()
                    {
                        if (e.Data != null)
                        {
                            OutputTextAppendLine(System.Text.Encoding.Default.GetString(System.Text.Encoding.UTF8.GetBytes(e.Data)));
                        }
                    });
                });
                if (dataReceivedEventHandler != null)
                {
                    p.OutputDataReceived += dataReceivedEventHandler;
                }

                // Événement lors de la réception de données sur la sortie d'erreur
                p.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
                {
                    Dispatcher.Invoke((Action)delegate ()
                    {
                        if (e.Data != null)
                        {
                            OutputTextAppendLine(System.Text.Encoding.Default.GetString(System.Text.Encoding.UTF8.GetBytes(e.Data)), OutputType.Error);
                        }
                    });
                });

                // Exécution de la commande dans un thread séparé
                ThreadStart ths = new ThreadStart(delegate ()
                {
                    Dispatcher.Invoke((Action)delegate ()
                    {
                        OutputTextAppendLine(p.StartInfo.FileName + "\" " + p.StartInfo.Arguments, OutputType.Command);
                    });

                    p.Start();

                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();

                    p.StandardInput.WriteLine("Ujasom9&");
                    p.WaitForExit();

                    if (p.ExitCode == 0)
                    {
                        Dispatcher.Invoke((Action)delegate ()
                        {
                            if (endAction != null)
                            {
                                endAction();
                            }
                            else
                            {
                                StatusTextBlock.Text = "Prêt";
                                IsWorking = false;
                                IsRepositoryReady = true;
                            }
                        });
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)delegate ()
                        {
                            StatusTextBlock.Text = "Prêt";
                            IsWorking = false;
                            IsRepositoryReady = true;
                        });
                    }

                    p.Close();
                });

                Thread th = new Thread(ths);
                th.Start();
            }
        }

        /// <summary>
        /// Ajoute une ligne de texte à la console de sortie.
        /// </summary>
        /// <param name="text">Le texte à ajouter.</param>
        /// <param name="type">Le type de texte.</param>
        public void OutputTextAppendLine(string text, OutputType type = OutputType.Standard)
        {
            Paragraph paragraph;

            switch (type)
            {
                case OutputType.Command:
                    paragraph = new Paragraph(new Run(">>> " + text));

                    paragraph.Foreground = new SolidColorBrush(Colors.Yellow);
                    paragraph.FontWeight = FontWeights.Bold;
                    paragraph.Margin = new Thickness(0, 15, 0, 5);

                    break;

                case OutputType.Error:
                    paragraph = new Paragraph(new Run(text));

                    paragraph.Foreground = new SolidColorBrush(Colors.Red);
                    paragraph.FontWeight = FontWeights.Bold;

                    break;

                default:
                    paragraph = new Paragraph(new Run(text));
                    break;
            }

            OutputTextBox.Document.Blocks.Add(paragraph);
            OutputTextBox.ScrollToEnd();
        }

        #region RefreshData

        /// <summary>
        /// Actualise les informations du dépôt sélectionné.
        /// </summary>
        public void RefreshSelectedRepositoryData()
        {
            BranchesComboBox.Items.Clear();
            BranchesToMergeComboBox.Items.Clear();

            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            if (repository.Path == null)
            {
                IsRepositoryReady = false;

                Title = "GitTool";
                SelectedRepositoryStatusBarItem.Visibility = Visibility.Collapsed;
                SelectedRepositoryTextBlock.Text = "";
            }
            else
            {
                IsWorking = true;
                IsRepositoryReady = false;

                Title = "GitTool - " + repository.Name;
                SelectedRepositoryTextBlock.Text = repository.Name;
                SelectedRepositoryStatusBarItem.Visibility = Visibility.Visible;

                Pull(() =>
                {
                    GetBranches(() =>
                    {
                        Status();
                    });
                });
            }
        }

        private void SetCurrentBranch(string branch = null)
        {
            if (branch == null)
            {
                branch = CurrentBranch;
            }

            Dispatcher.Invoke((Action)delegate ()
            {
                int index = BranchesComboBox.Items.IndexOf(branch);
                if (index > -1)
                {
                    BranchesComboBox.SelectedIndex = index;
                }
            });
        }

        private void BuildBranchesToMergeComboBox()
        {
            object selectedValue = BranchesToMergeComboBox.SelectedValue;

            BranchesToMergeComboBox.Items.Clear();

            for (int i = 0; i < BranchesComboBox.Items.Count; i++)
            {
                if (i != BranchesComboBox.SelectedIndex)
                {
                    BranchesToMergeComboBox.Items.Add(BranchesComboBox.Items[i]);
                }
            }

            BranchesToMergeComboBox.SelectedValue = selectedValue;
        }

        private void GetBranches(Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Récupération des branches du dépôt \"{0}\"...", repository.Name);

            BranchesComboBox.Items.Clear();
            BranchesToMergeComboBox.Items.Clear();
            ExecGitCommand(repository, "ls-remote --heads", null, Branches_DataReceived, endAction, statusLabel);
        }

        private void Branches_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                Regex regex = new Regex(@"refs/heads/(.+)");
                Match match = regex.Match(e.Data);
                if (match.Success)
                {
                    string branch = match.Groups[1].ToString();
                    if (!branch.Equals(BRANCH_MASTER) && !branch.Equals(BRANCH_STAGING))
                    {
                        Dispatcher.Invoke((Action)delegate ()
                        {
                            BranchesComboBox.Items.Add(branch);
                        });
                    }
                }
            }
        }

        private void Status(Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Récupération de l’état du dépôt \"{0}\"...", repository.Name);

            ExecGitCommand(repository, "status", null, Status_DataReceived, endAction, statusLabel);
        }

        private void Status_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                Regex regex = new Regex(@"^On branch (.+)$");
                Match match = regex.Match(e.Data);
                if (match.Success)
                {
                    CurrentBranch = match.Groups[1].ToString();
                }
            }
        }

        #endregion

        #region RenameBranch

        public void RenameCurrentBranch(string name)
        {
            string currentName = BranchesComboBox.SelectedValue.ToString();

            if (!name.Equals("") && !currentName.Equals("") && !name.Equals(currentName))
            {
                IsWorking = true;
                IsRepositoryReady = false;

                Checkout(currentName, () =>
                {
                    RenameBranch(currentName, name, () =>
                    {
                        RefreshSelectedRepositoryData();
                    });
                });
            }
        }

        private void RenameBranch(string oldName, string newName, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string command = "branch -m " + oldName + " " + newName;
            string statusLabel = String.Format("Renommage de la branche \"{0}\" du dépôt \"{1}\" en \"{2}\"...", oldName, repository.Name, newName);

            ExecGitCommand(repository, command, null, null, () =>
            {
                PushBranch(oldName, () =>
                {
                    SetUpStream(newName, endAction);
                });
            }, statusLabel);
        }

        private void PushBranch(string name, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string command = String.Format("push origin :{0}", name);
            string statusLabel = String.Format("Suppression de la branche \"{0}\" du dépôt \"{1}\" ...", name, repository.Name);

            ExecGitCommand(repository, command, null, null, endAction);
        }

        private void SetUpStream(string name, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string command = String.Format("push --set-upstream origin {0}", name);
            string statusLabel = String.Format("Envoi de la branche \"{0}\" du dépôt \"{1}\"...", name, repository.Name);

            ExecGitCommand(repository, command, null, null, endAction);
        }

        #endregion

        #region MergeBranch

        private void MergeOnStaging()
        {
            string branch = BranchesComboBox.SelectedValue.ToString();
            if (!branch.Equals(""))
            {
                IsWorking = true;
                IsRepositoryReady = false;

                Checkout(BRANCH_STAGING, () => {
                    MergeBranch(branch, false, true);
                });
            }
        }

        private void MergeOnHotfixes()
        {
            string branch = BranchesComboBox.SelectedValue.ToString();
            if (!branch.Equals(""))
            {
                IsWorking = true;
                IsRepositoryReady = false;

                Checkout(BRANCH_HOTFIXES, () =>
                {
                    MergeBranch(branch, false, true);
                });
            }
        }

        private void MergeOnMaster()
        {
            string branch = BranchesComboBox.SelectedValue.ToString();
            if (!branch.Equals(""))
            {
                IsWorking = true;
                IsRepositoryReady = false;

                Checkout(BRANCH_MASTER, () =>
                {
                    Patch(branch, BRANCH_MASTER, () =>
                    {
                        Add(() =>
                        {
                            Commit(CommitMessageTextBox.Text);
                        });
                    });
                });
            }
        }

        private void MergeOnWorking()
        {
            string sourceBranch = BranchesComboBox.SelectedValue.ToString();
            string destBranch = BranchesToMergeComboBox.SelectedValue.ToString();
            if (!sourceBranch.Equals("") && !destBranch.Equals(""))
            {
                IsWorking = true;
                IsRepositoryReady = false;

                Checkout(destBranch, () => {
                    MergeBranch(sourceBranch, false, false);
                });
            }
        }

        #endregion

        #region Tags

        private void CreateTag()
        {
            IsWorking = true;
            IsRepositoryReady = false;

            string tag = String.Format("v{0}", Version);
            string message = String.Format("Version {0}", Version);

            Checkout(BRANCH_MASTER, () => {
                Tag(tag, message);
            });
        }

        #endregion

        #region Config

        private void SetConfigValue(string name, string value)
        {
            IsWorking = true;
            IsRepositoryReady = false;

            Config(name, value);
        }

        private void UnsetConfigValue(string name)
        {
            IsWorking = true;
            IsRepositoryReady = false;

            UnsetConfig(name);
        }

        #endregion

        #region CommonCommands

        private void Checkout(string branch, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Passage du dépôt \"{0}\" sur la branche \"{1}\"...", repository.Name, branch);

            string command = String.Format("checkout {0}", branch);
            ExecGitCommand(repository, command, null, null, () => { Pull(endAction); }, statusLabel);
        }

        private void MergeBranch(string branch, bool isSquash = false, bool isTheirs = false, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Fusion de la branche \"{0}\" du dépôt \"{1}\"...", branch, repository.Name);

            string command;
            if (isSquash)
            {
                if (isTheirs)
                {
                    command = String.Format("merge -X theirs --squash {0}", branch);
                }
                else
                {
                    command = String.Format("merge --squash {0}", branch);
                }
            }
            else
            {
                if (isTheirs)
                {
                    command = String.Format("merge -X theirs {0}", branch);
                }
                else
                {
                    command = String.Format("merge {0}", branch);
                }
            }
            ExecGitCommand(repository, command, null, null, endAction, statusLabel);
        }

        private void Patch(string fromBranch, string toBranch, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Patch de la branche \"{0}\" vers la branche \"{1}\" du dépôt \"{2}\"...", fromBranch, toBranch, repository.Name);

            string branchesDiff = String.Format("{0}..{1}", toBranch, fromBranch);
            string gitExe = Properties.Settings.Default.GitExe.Replace("\\", "\\\\");
            string command = String.Format("diff --binary {0} | \"{1}\" apply", branchesDiff, gitExe);

            Action patchDiffSummaryEndAction = () =>
            {
                string updIndexCommand = "update-index --chmod={0} {1}";

                ExecGitCommand(repository, String.Format(updIndexCommand, "+x", string.Join(" ", chmod["add"].ToArray())), null, null, () => 
                {
                    ExecGitCommand(repository, String.Format(updIndexCommand, "-x", string.Join(" ", chmod["remove"].ToArray())), null, null, endAction);
                });
            };

            Action newEndAction = () =>
            {
                ExecGitCommand(repository, String.Format("diff --summary {0}", branchesDiff), null, PatchDiffSummary_DataReceived, patchDiffSummaryEndAction);
            };

            ExecGitCommand(repository, command, null, null, newEndAction, statusLabel);
        }

        private void PatchDiffSummary_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                Regex regex = new Regex(@"mode change (100644 => 100755|100755 => 100644) (.+)$");
                Match match = regex.Match(e.Data);
                if (match.Success)
                {
                    string key = null;

                    if (match.Groups[1].ToString().Equals("100644 => 100755"))
                    {
                        key = "add";
                    }

                    if (match.Groups[1].ToString().Equals("100755 => 100644"))
                    {
                        key = "remove";
                    }

                    if (key != null)
                    {
                        chmod[key].Add(match.Groups[2].ToString());
                    }
                }
            }
        }

        private void Add(Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Ajout des fichiers à l'index du dépôt \"{0}\"...", repository.Name);

            string command = "add .";
            ExecGitCommand(repository, command, null, null, endAction, statusLabel);
        }

        private void Commit(string message = null, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Commit sur le dépôt \"{0}\"...", repository.Name);

            string command;
            if (message != null && !message.Trim().Equals(""))
            {
                command = String.Format("commit -m \"{0}\"", message.Trim());
            }
            else
            {
                command = "commit";
            }
            ExecGitCommand(repository, command, null, null, endAction, statusLabel);
        }

        private void Tag(string name, string message = null, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Création du tag \"{0}\" sur le dépôt \"{1}\"...", name, repository.Name);

            string command;
            if (message != null && !message.Trim().Equals(""))
            {
                command = String.Format("tag -a {0} -m \"{1}\"", name, message.Trim());
            }
            else
            {
                command = String.Format("tag -a {0}", name);
            }
            ExecGitCommand(repository, command, null, null, endAction, statusLabel);
        }

        private void Pull(Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Mise à jour du dépôt \"{0}\"...", repository.Name);

            ExecGitCommand(repository, "pull", null, null, endAction, statusLabel);
        }

        private void Push(Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Envoi des modifications sur le dépôt \"{0}\"...", repository.Name);

            ExecGitCommand(repository, "push", null, null, endAction, statusLabel);
        }

        private void PushTag(string name, Action endAction = null)
        {
            Repository repository = RepositoriesTreeView.SelectedItem as Repository;

            string statusLabel = String.Format("Envoi du tag \"{0}\" sur le dépôt \"{1}\"...", name, repository.Name);

            string command = String.Format("push origin {0}", name);
            ExecGitCommand(repository, command, null, null, endAction, statusLabel);
        }

        private void Config(string name, string value, Action endAction = null)
        {
            string statusLabel = String.Format("Mise à jour de la configuration \"{0}\"...", name);

            string command = String.Format("config --global {0} \"{1}\"", name, value);
            ExecGitCommand(null, command, null, null, endAction, statusLabel);
        }

        private void UnsetConfig(string name, Action endAction = null)
        {
            string statusLabel = String.Format("Suppression de la configuration \"{0}\"...", name);

            string command = String.Format("config --global --unset {0}", name);
            ExecGitCommand(null, command, null, null, endAction, statusLabel);
        }

        #endregion

        #region Events

        private void RepositoriesTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            RefreshSelectedRepositoryData();
        }

        private void BranchesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string extractedVersion = "";

            ComboBox comboBox = sender as ComboBox;

            if (comboBox.SelectedValue != null)
            {
                Regex regex = new Regex(@"^dev(\d+\.\d+\.\d+([a-z]*?|-p\d+)?(-beta\d+)?)$");
                Match match = regex.Match(comboBox.SelectedValue.ToString());
                if (match.Success)
                {
                    extractedVersion = match.Groups[1].ToString();
                }
            }

            Version = extractedVersion;
            if (comboBox.SelectedValue == null || comboBox.SelectedValue.ToString().Equals(""))
            {
                currentBranch = "";
            }
            else
            {
                currentBranch = comboBox.SelectedValue.ToString();
            }

            BuildBranchesToMergeComboBox();
        }

        private void VersionTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (VersionTextBox.Text.Trim().Equals(""))
            {
                CommitMessageTextBox.Text = "";
            }
            else
            {
                CommitMessageTextBox.Text = String.Format("Mise en exploitation de la version {0}", VersionTextBox.Text.Trim());
            }
        }

        private void OptionsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OptionsWindow optionsWindow = new OptionsWindow();
            optionsWindow.Owner = this;
            optionsWindow.ShowDialog();
        }

        private void RenameBranchButton_Click(object sender, RoutedEventArgs e)
        {
            RenameWindow renameWindow = new RenameWindow();
            renameWindow.Owner = this;
            renameWindow.CurrentBranch = BranchesComboBox.SelectedValue.ToString();
            renameWindow.ShowDialog();
        }

        private void Confirm(string caption, string message, Action action = null)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show(message, caption, MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                action?.Invoke();
            }
        }

        private void ConfirmMerge(string sourceBranch, string destBranch, Action action = null)
        {
            string caption = String.Format("Fusion sur la branche \"{0}\"", destBranch);
            string message = String.Format("Vous allez fusionner la branche \"{0}\" avec la branche \"{1}\". Souhaitez-vous continuer ?", sourceBranch, destBranch);

            Confirm(caption, message, action);
        }

        private void MergeWithStagingButton_Click(object sender, RoutedEventArgs e)
        {
            ConfirmMerge(BranchesComboBox.SelectedValue.ToString(), BRANCH_STAGING, MergeOnStaging);
        }

        private void MergeWithMasterButton_Click(object sender, RoutedEventArgs e)
        {
            ConfirmMerge(BranchesComboBox.SelectedValue.ToString(), BRANCH_MASTER, MergeOnMaster);
        }

        private void CreateTagButton_Click(object sender, RoutedEventArgs e)
        {
            string caption = "Création de tag";
            string message = String.Format("Vous allez créer le tag \"v{0}\". Souhaitez-vous continuer ?", Version);

            Confirm(caption, message, CreateTag);
        }

        private void PushTagButton_Click(object sender, RoutedEventArgs e)
        {
            string tag = String.Format("v{0}", Version);
            PushTag(tag, () => { });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OutputTextBox.Document.Blocks.Clear();
        }

        private void PushStagingButton_Click(object sender, RoutedEventArgs e)
        {
            DoPushAction(BRANCH_STAGING);
        }

        private void PushMasterButton_Click(object sender, RoutedEventArgs e)
        {
            DoPushAction(BRANCH_MASTER);
        }

        private void DoPushAction(string branch)
        {
            IsWorking = true;
            IsRepositoryReady = false;

            Checkout(branch, () => {
                Push(() =>
                {
                    IsWorking = false;
                    IsRepositoryReady = true;
                });
            });
        }

        private void MergeStagingWithHotfixesButton_Click(object sender, RoutedEventArgs e)
        {
            ConfirmMerge(BranchesComboBox.SelectedValue.ToString(), BRANCH_HOTFIXES, MergeOnHotfixes);
        }

        private void PushHotfixesButton_Click(object sender, RoutedEventArgs e)
        {
            DoPushAction(BRANCH_HOTFIXES);
        }

        private void MergeWorkingButton_Click(object sender, RoutedEventArgs e)
        {
            ConfirmMerge(BranchesComboBox.SelectedValue.ToString(), BranchesToMergeComboBox.SelectedValue.ToString(), MergeOnWorking);
        }

        private void PushWorkingButton_Click(object sender, RoutedEventArgs e)
        {
            String workingBranch = "";

            if (BranchesToMergeComboBox.SelectedValue != null)
            {
                workingBranch = BranchesToMergeComboBox.SelectedValue.ToString();

                if (!workingBranch.Equals(""))
                {
                    DoPushAction(workingBranch);
                }
            }
        }

        private void UpdateGlobalConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SetConfigValue(CONFIG_AUTOCRLF, "false");
            SetConfigValue(CONFIG_USER_EMAIL, Properties.Settings.Default.UserEmail);
            SetConfigValue(CONFIG_USER_NAME, Properties.Settings.Default.UserName);
            UnsetConfigValue(CONFIG_BRANCH_AUTOSETUPREBASE);
            UnsetConfigValue(CONFIG_PULL_REBASE);
        }

        private void DryRunMenuItem_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    #endregion

    #region Converters

    public class OppositeBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                return (!(bool)value);
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class VersionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] is bool && values[1] is Repository)
            {
                return ((bool)values[0] && ((Repository)values[1]).Path != null);
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BranchSelectedConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] is bool && values[1] is int)
            {
                return (!(bool)values[0] && (int)values[1] != -1);
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CanMergeOnMasterConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] is bool && values[1] is string)
            {
                return ((bool)values[0] && !((string)values[1]).Trim().Equals(""));
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CanCreateTagConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] is bool && values[1] is string)
            {
                return ((bool)values[0] && !((string)values[1]).Trim().Equals(""));
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CanMergeOnWorkingConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] is bool && values[1] is int && values[2] is int)
            {
                return (!(bool)values[0] && (int)values[1] != -1 && (int)values[2] != -1);
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DryRunConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                return ((bool)value ? "Dry run activé" : "Dry run désactivé");
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    #endregion
}
