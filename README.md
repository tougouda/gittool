# GitTool

Lors des mises en recette et des mises en production, les développeurs doivent réaliser un certain nombre d’opérations sur le dépôt Git :
* pour une mise en recette :
  * fusion de la branche de version sur la branche *staging* ;
* pour une mise en production :
  * fusion de la branche de version sur la branche *master* ;
  * création du tag de version ;
  * fusion de la branche de version sur la branche *hotfix* et les branches de développement.

GitTool permet à tous les développeurs de l’équipe de réaliser ces opérations de manière simple et homogène (en utilisant tous les mêmes commandes et les mêmes paramètres).

![Capture d’écran](images/screenshot.png)
